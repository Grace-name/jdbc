package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dao.UserDao;
import dao.impl.UserDaoImpl;
import util.JDBCUtil;

public class JDBCUtilTest {
	JDBCUtil jdbc = null;
	@Before
	public void setUp() throws Exception {
		jdbc = new JDBCUtil();
	}

	@After
	public void tearDown() throws Exception {
		jdbc = null;
	}

	@Test
	public void testGetConnection() {
	}

	@Test
	public void testCloseAll() {
	}

	@Test
	public void testExecuteUpdate() {
		String sql =  "insert into user values(null,?,?,?,?,?)";
		int num = jdbc.executeUpdate(sql, "����","wangwu","123","��","1234565677");
		
		assertEquals(num, 1);
	}

//	@Test
//	public void testExecuteQuery() {
//		UserDao ud = new UserDaoImpl();
//		List<User> list = ud.findAll();		
//	}

}
