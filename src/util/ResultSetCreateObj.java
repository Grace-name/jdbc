package util;

import java.sql.ResultSet;

public interface ResultSetCreateObj <T> {
	/**
	 * 结果集对象创建
	 * @param <T>
	 * @param rs
	 * @return
	 */
	T create(ResultSet rs);
}
