package dao;

import java.util.List;

import entity.User;
/**
 * 数据库操作接口
 * @author qq198
 *
 * @param <T>
 */
public interface UserDao {
	/**
	 * 添加用户
	 * @param user
	 * @return
	 */
	int add(User user);
	/**
	 * 修改用户
	 * @param user
	 * @return
	 */
	int update(User user);
	/**
	 * 删除用户
	 * @param id
	 * @return
	 */
	int delete(int id);
	/**
	 * 展示所有用户
	 * @return
	 */
	List<User> findAll();
	/**
	 * 通过用户名模糊查询
	 * @param name用户名
	 * @return
	 */
	List<User> findLikeName(String name);
}
