package service;

import java.util.List;

import entity.User;

public interface UserService {
	boolean add(User user);
	boolean update(User user);
	boolean delete(int id);
	List<User> findAll();
	List<User> findLikeName(String name);
}
