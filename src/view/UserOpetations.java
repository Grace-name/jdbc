package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import entity.User;
import service.UserService;
import service.impl.UserServiceImpl;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.border.EtchedBorder;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class UserOpetations extends JFrame {

	private JPanel contentPane;
	private JTextField addName;
	private JTextField addUsername;
	private JPasswordField addPwd;
	private JTextField addTel;
	private JTextField deleteText;
	private JTextField updateId;
	private JTextField updateUsername;
	private JPasswordField updatePwd;
	private JTextField updateTel;
	private UserService us = new UserServiceImpl();
	private JTextField searchName;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserOpetations frame = new UserOpetations();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UserOpetations() {
		setTitle("\u7528\u6237\u64CD\u4F5C");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 620, 630);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 10, 584, 91);
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "\u589E", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u59D3\u540D");
		lblNewLabel.setBounds(21, 24, 44, 15);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u6027\u522B");
		lblNewLabel_1.setBounds(21, 55, 44, 15);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u7528\u6237\u540D");
		lblNewLabel_2.setBounds(190, 24, 58, 15);
		panel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("\u5BC6\u7801");
		lblNewLabel_3.setBounds(341, 24, 44, 15);
		panel.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("\u624B\u673A\u53F7");
		lblNewLabel_4.setBounds(243, 55, 58, 15);
		panel.add(lblNewLabel_4);
		
		addName = new JTextField();
		addName.setBounds(59, 21, 66, 21);
		panel.add(addName);
		addName.setColumns(10);
		
		addUsername = new JTextField();
		addUsername.setBounds(233, 21, 66, 21);
		panel.add(addUsername);
		addUsername.setColumns(10);
		
		addPwd = new JPasswordField();
		addPwd.setBounds(375, 21, 66, 21);
		panel.add(addPwd);
		
		JRadioButton addGenderM = new JRadioButton("\u7537");
		addGenderM.setBounds(57, 51, 58, 23);
		panel.add(addGenderM);
		//默认男性
		addGenderM.setSelected(true);
		
		
		JRadioButton addGenderW = new JRadioButton("\u5973");
		addGenderW.setBounds(117, 51, 66, 23);
		panel.add(addGenderW);
		
		//设置单选只能选一个
		ButtonGroup bg = new ButtonGroup();
		bg.add(addGenderW);
		bg.add(addGenderM);
		
		addTel = new JTextField();
		addTel.setBounds(335, 52, 106, 21);
		panel.add(addTel);
		addTel.setColumns(10);
		
		JButton btnNewButton = new JButton("\u6DFB\u52A0");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//添加操作
				String name = addName.getText();
				String username = addUsername.getText();
				String pwd = addPwd.getPassword().toString();
				String tel = addTel.getText();
				String gender = "";
				if(addGenderM.isSelected()) {
					gender = "男";
					System.out.println("男");
				}else {
					gender = "女";
				}
				User user = new User();
				user.setName(name);
				user.setUsername(username);
				user.setGender(gender);
				user.setPassword(pwd);
				user.setTelephone(tel);
				if(us.add(user)) {
					JOptionPane.showMessageDialog(UserOpetations.this, "恭喜您，添加成功", "提示",JOptionPane.INFORMATION_MESSAGE);
					addName.setText("");
					addUsername.setText("");
					addPwd.setText("");
					addTel.setText("");
					addGenderM.setSelected(true);
					List<User> list = us.findAll();
					initTable(list);
				}else {
					JOptionPane.showMessageDialog(UserOpetations.this, "很遗憾，添加失败", "错误",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnNewButton.setBounds(488, 39, 74, 28);
		panel.add(btnNewButton);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 111, 584, 46);
		panel_1.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "\u5220", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel_5 = new JLabel("\u7F16\u53F7");
		lblNewLabel_5.setBounds(42, 21, 58, 15);
		panel_1.add(lblNewLabel_5);
		
		deleteText = new JTextField();
		deleteText.setText("");
		deleteText.setBounds(135, 18, 134, 21);
		panel_1.add(deleteText);
		deleteText.setColumns(10);
		
		JButton btnNewButton_1 = new JButton("\u5220\u9664");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//删除操作
				String idstr = deleteText.getText();
				int id = Integer.parseInt(idstr);
				if(us.delete(id)) {
					JOptionPane.showMessageDialog(UserOpetations.this, "恭喜您，删除成功", "提示",JOptionPane.INFORMATION_MESSAGE);
					deleteText.setText("");
					List<User> list = us.findAll();
					initTable(list);
				}else {
					JOptionPane.showMessageDialog(UserOpetations.this, "很遗憾，删除失败", "错误",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnNewButton_1.setBounds(488, 13, 74, 23);
		panel_1.add(btnNewButton_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(10, 167, 584, 91);
		panel_2.setLayout(null);
		panel_2.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "\u6539", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		contentPane.add(panel_2);
		
		JLabel lblNewLabel_6 = new JLabel("\u7F16\u53F7");
		lblNewLabel_6.setBounds(21, 24, 44, 15);
		panel_2.add(lblNewLabel_6);
		
		JLabel lblNewLabel_1_1 = new JLabel("\u6027\u522B");
		lblNewLabel_1_1.setBounds(21, 55, 44, 15);
		panel_2.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_2_1 = new JLabel("\u7528\u6237\u540D");
		lblNewLabel_2_1.setBounds(190, 24, 58, 15);
		panel_2.add(lblNewLabel_2_1);
		
		JLabel lblNewLabel_3_1 = new JLabel("\u5BC6\u7801");
		lblNewLabel_3_1.setBounds(341, 24, 44, 15);
		panel_2.add(lblNewLabel_3_1);
		
		JLabel lblNewLabel_4_1 = new JLabel("\u624B\u673A\u53F7");
		lblNewLabel_4_1.setBounds(243, 55, 58, 15);
		panel_2.add(lblNewLabel_4_1);
		
		updateId = new JTextField();
		updateId.setColumns(10);
		updateId.setBounds(59, 21, 66, 21);
		panel_2.add(updateId);
		
		updateUsername = new JTextField();
		updateUsername.setColumns(10);
		updateUsername.setBounds(233, 21, 66, 21);
		panel_2.add(updateUsername);
		
		updatePwd = new JPasswordField();
		updatePwd.setBounds(375, 21, 66, 21);
		panel_2.add(updatePwd);
		
		JRadioButton updateGerdenM = new JRadioButton("\u7537");
		updateGerdenM.setSelected(true);
		updateGerdenM.setBounds(57, 51, 58, 23);
		panel_2.add(updateGerdenM);
		
		JRadioButton updateGerdenW = new JRadioButton("\u5973");
		updateGerdenW.setBounds(117, 51, 66, 23);
		panel_2.add(updateGerdenW);
		
		ButtonGroup bg2 = new ButtonGroup();
		bg2.add(updateGerdenW);
		bg2.add(updateGerdenM);
		
		updateTel = new JTextField();
		updateTel.setColumns(10);
		updateTel.setBounds(335, 52, 106, 21);
		panel_2.add(updateTel);
		
		JButton btnNewButton_2 = new JButton("\u4FEE\u6539");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//修改操作
				int id = Integer.parseInt(updateId.getText());
				String username = updateUsername.getText();
				String pwd = updatePwd.getPassword().toString();
				String tel = updateTel.getText();
				String gender = "";
				if(updateGerdenM.isSelected()) {
					gender = "男";
				}else {
					gender = "女";
				}
				User user = new User();
				user.setId(id);
				user.setUsername(username);
				user.setGender(gender);
				user.setPassword(pwd);
				user.setTelephone(tel);
				if(us.update(user)) {
					JOptionPane.showMessageDialog(UserOpetations.this, "恭喜您，修改成功", "提示",JOptionPane.INFORMATION_MESSAGE);
					updateId.setText("");
					updateUsername.setText("");
					updatePwd.setText("");
					updateTel.setText("");
					List<User> list = us.findAll();
					initTable(list);
					updateGerdenM.setSelected(true);
				}else {
					JOptionPane.showMessageDialog(UserOpetations.this, "很遗憾，修改失败", "错误",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnNewButton_2.setBounds(488, 39, 74, 28);
		panel_2.add(btnNewButton_2);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 332, 584, 225);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"\u7F16\u53F7", "\u59D3\u540D", "\u7528\u6237\u540D", "\u6027\u522B", "\u624B\u673A\u53F7"
			}
		));
		scrollPane.setViewportView(table);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "\u67E5", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLACK));
		panel_3.setBounds(10, 268, 584, 54);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblNewLabel_7 = new JLabel("\u7528\u6237\u540D");
		lblNewLabel_7.setBounds(35, 29, 58, 15);
		panel_3.add(lblNewLabel_7);
		
		searchName = new JTextField();
		searchName.setBounds(138, 26, 132, 21);
		panel_3.add(searchName);
		searchName.setColumns(10);
		
		JButton btnNewButton_3 = new JButton("\u67E5\u8BE2");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//模糊查询
				String name = searchName.getText();
				List<User> users = us.findLikeName(name);
				initTable(users);
			}
		});
		btnNewButton_3.setBounds(333, 25, 74, 23);
		panel_3.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("\u663E\u793A\u6240\u6709");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<User> list = us.findAll();
				initTable(list);
			}
		});
		btnNewButton_4.setBounds(460, 25, 102, 23);
		panel_3.add(btnNewButton_4);
		
		List<User> list = us.findAll();
		initTable(list);
	}
	/**
	 * 设置表格的内容
	 * @param list
	 */
	private void initTable(List<User> list) {
		
		DefaultTableModel dm = (DefaultTableModel) table.getModel();
		int count = dm.getRowCount();
		for (int i = 0; i < count; i++) {
			dm.removeRow(0);
		}
		
		for (User user : list) {
			dm.addRow(new Object[] {
					user.getId(),
					user.getName(),
					user.getUsername(),
					user.getGender(),
					user.getTelephone()
			});
			System.out.println();

		}
	}
}
