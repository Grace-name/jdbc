package service.impl;

import java.util.List;

import dao.UserDao;
import dao.impl.UserDaoImpl;
import entity.User;
import service.UserService;

public class UserServiceImpl implements UserService {
	UserDao ud = new UserDaoImpl();
	@Override
	public boolean add(User user) {
		return ud.add(user)>0;
	}

	@Override
	public boolean update(User user) {
		// TODO Auto-generated method stub
		return ud.update(user)>0;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return ud.delete(id)>0;
	}

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return ud.findAll();
	}

	@Override
	public List<User> findLikeName(String name) {
		// TODO Auto-generated method stub
		return ud.findLikeName(name);
	}

}
