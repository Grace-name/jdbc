package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class JDBCUtil {
	private static final String DRIVER="com.mysql.cj.jdbc.Driver";
	private static final String URL="jdbc:mysql://localhost:3306/users?serverTimezone=GMT%2B8";
	private static final String USER="root";
	private static final String PASSWORD="123456";
	static {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * 获取数据库连接
	 * @return
	 */
	public static Connection getConnection (){
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
	/**
	 * 关闭数据库
	 * @param rs
	 * @param st
	 * @param conn
	 */
	public static void closeAll(ResultSet rs, Statement st, Connection conn) {
		try {
			if(rs!=null) {
				rs.close();
				rs = null;
			}
			if(st!=null) {
				st.close();
			}
			if(conn!=null) {
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * 增删改
	 * @param sql执行的语句
	 * @param params设置参数
	 * @return是否成功
	 */
	public static int executeUpdate(String sql,Object... params) {
		int num = 0;
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			if(params != null) {
				for (int i = 0; i < params.length; i++) {
					ps.setObject(i+1, params[i]);
				}
			}
			num = ps.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			closeAll(null, ps, conn);
		}
		
		return num;
	}
	/**
	 * 查询
	 * @param <T>
	 * @param sql
	 * @param rsco
	 * @param param
	 * @return
	 */
	public static <T> List<T> executeQuery(String sql,ResultSetCreateObj<T> rsco,Object...param){
		List<T> list = new ArrayList();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement(sql);
			if(param!=null) {
				for (int i = 0; i < param.length; i++) {
					ps.setObject(i+1, param[i]);
				}
			}
			rs = ps.executeQuery();
			while(rs.next()) {
				T obj = rsco.create(rs);
				list.add(obj);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			closeAll(rs, ps, conn);
		}
		return list;
		
	}
}
